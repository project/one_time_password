<?php

namespace Drupal\one_time_password;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Access\AccessResultInterface;
use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\Entity\EntityDefinitionUpdateManagerInterface;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Manage attaching the new field to the user entity.
 */
final class UserFieldAttach implements ContainerInjectionInterface {

  use StringTranslationTrait;

  /**
   * Create an instance of UserFieldAttach.
   */
  public function __construct(
    protected EntityDefinitionUpdateManagerInterface $entityDefinitionUpdateManager,
  ) {}

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container): UserFieldAttach {
    return new static(
      $container->get(EntityDefinitionUpdateManagerInterface::class),
    );
  }

  /**
   * Get the base field definition for the one time password field.
   *
   * @return \Drupal\Core\Field\BaseFieldDefinition
   *   A base field definition for the one time password field.
   */
  protected function getBaseFieldDefinition(): BaseFieldDefinition {
    return BaseFieldDefinition::create('one_time_password_provisioning_uri')
      ->setLabel($this->t('Two-Factor Authentication'))
      ->setName('one_time_password')
      ->setProvider('one_time_password')
      ->setTargetEntityTypeId('user')
      ->setDescription($this->t('Setup a two-factor authentication.'))
      ->setDisplayConfigurable('view', FALSE)
      ->setDisplayConfigurable('form', FALSE)
      ->setDisplayOptions('view', [
        'region' => 'hidden',
      ])
      ->setDisplayOptions('form', [
        'region' => 'hidden',
      ]);
  }

  /**
   * Implements hook_entity_base_field_info_alter().
   */
  public function entityBaseFieldInfoAlter(&$fields, EntityTypeInterface $entity_type): void {
    if ($entity_type->id() !== 'user') {
      return;
    }
    $fields['one_time_password'] = $this->getBaseFieldDefinition();
  }

  /**
   * Install the field definition.
   */
  public function installFieldDefinition(): void {
    $this->entityDefinitionUpdateManager->installFieldStorageDefinition('one_time_password', 'user', 'user', $this->getBaseFieldDefinition());
  }

  /**
   * Implements hook_entity_field_access().
   */
  public function entityFieldAccess($operation, FieldDefinitionInterface $field_definition, AccountInterface $account, ?FieldItemListInterface $items = NULL): AccessResultInterface {
    if ($field_definition->getName() === 'one_time_password') {
      return AccessResult::forbidden();
    }
    return AccessResult::neutral();
  }

}
